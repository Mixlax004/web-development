let table = document.getElementById("my_table");
function addData(ary){
    var results_amount = ary.length;
    var results = document.getElementById("results_amount");
    results.innerHTML = "(Total " + results_amount + ")";

    i=0;
    if (Object.keys(ary).length != 0){
        while (i < 10 && i < ary.length) {
            var row = table.insertRow(-1);
            var fecha = row.insertCell(0);
            var sexo = row.insertCell(1);
            var atendido_por = row.insertCell(2);
            var peso = row.insertCell(3);
            
            fecha.innerText = ary[i].fecha_nacimiento.slice(0,10);
            sexo.innerText = ary[i].sexo;
            atendido_por.innerText = ary[i].parto_atendido_por;
            peso.innerText = ary[i].peso_gramos;
            i++;
          }
    }

}

function deleteData(){
    console.log(table.rows.length);
    for(var i=table.rows.length;i>1;i--){
        table.deleteRow(-1);
    }
}

function consult() {
    deleteData();

    var anio = document.getElementById("anio").value;
    var rh = document.getElementById("rh").value;
    var edad_madre = document.getElementById("edad_madre").value;
    var base_url = "https://www.datos.gov.co/resource/9rty-i39a.json";
    var url1 = "";
    var url2 = "";
    var url3 = "";
    if(anio!=""){
        url1 = base_url+"?a_o_reporte="+anio;
    } else {
        url1 = base_url;
    }
    if(rh!=""){
        url2 = url1+"&factor_rh="+rh;
    } else {
        url2 = url1;
    }
    if(edad_madre!=""){
        url3 = url2+"&edad_madre="+edad_madre;
    } else {
        url3 = url2;
    }

    fetch(url3).
    then(respuesta=>{return respuesta.json()})
    .then(res=>{addData(res)})

}

// Count elements of a list
function addItem(inputValue){
    var li = document.createElement("li");
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue == '') {
        alert("Please write something");
    } else {
        document.getElementById("consultList").appendChild(li);
    }
    document.getElementById("consult").value = "";
}