function consult() {
    clearAll();

    var inputValue = document.getElementById("consult").value;
    var base_url = "https://www.datos.gov.co/resource/gt2j-8ykr.json";
    var final_url = "";
    if(inputValue!=""){
        final_url = base_url+"?departamento="+inputValue;
    } else {
        final_url = base_url;
    }

    fetch(final_url).
    then(respuesta=>{return respuesta.json()})
    .then(res=>{placeValues(res)})

}

function clearAll() {
    var ul = document.getElementById("consultList")
    while (ul.firstChild) {
    ul.removeChild(ul.firstChild);
    }
 }

// Count elements of a list
function addItem(inputValue){
    var li = document.createElement("li");
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue == '') {
        alert("Please write something");
    } else {
        document.getElementById("consultList").appendChild(li);
    }
    document.getElementById("consult").value = "";
}

function placeValues(ary){
    i=0;
    if (Object.keys(ary).length == 0){
        alert("No items to show")
    } else {
        console.log(ary.lenght)
        while (i < 10 && i < ary.length) {
            text = ary[i].departamento_nom + ", " +  ary[i].ciudad_municipio_nom + ", " + ary[i].tipo_recuperacion + ", " + ary[i].ubicacion;
            addItem(text);
            i++;
          }
    }
}