// Setting values to zeros
var hours = 09;
var mins = 59;
var secs = 50;

// Getting time elements
var appendHours = document.getElementById("hours");
var appendMins = document.getElementById("mins");
var appendSecs = document.getElementById("secs");

// Getting button elements
var buttonStart = document.getElementById("btn-start");
var buttonStop = document.getElementById("btn-stop");
var buttonReset = document.getElementById("btn-reset");

var Interval;

buttonStart.onclick = function() {
    clearInterval(Interval);
    Interval = setInterval(startTimer, 1000);
}

buttonStop.onclick = function() {
    clearInterval(Interval);
}

buttonReset.onclick = function() {
    clearInterval(Interval);
    hours = "00";
    mins = "00";
    secs = "00";
    appendHours.innerHTML = hours;
    appendMins.innerHTML = mins;
    appendSecs.innerHTML = secs;
}

function startTimer () {
    secs++; 
    
    if(secs <= 9){
      appendSecs.innerHTML = "0" + secs;
    }
    
    if (secs > 9){
      appendSecs.innerHTML = secs;
      
    } 
    
    if (secs > 59) {
      mins++;
      appendMins.innerHTML = "0" + mins;
      secs = 0;
      appendSecs.innerHTML = "0" + 0;
    }
    
    if (mins > 9){
      appendMins.innerHTML = mins;
    }
  
    if (mins > 59) {
      hours++;
      appendHours.innerHTML = "0" + hours;
      mins = 0;
      appendMins.innerHTML = "0" + 0;
    }

    if (hours > 9){
      appendHours.innerHTML = hours;
    }
  }

