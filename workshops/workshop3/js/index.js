// Create a new list item when clicking on the add button
function newElement() {
    var li = document.createElement("li");
    var inputValue = document.getElementById("listInput").value;
    var t = document.createTextNode(inputValue);
    li.appendChild(t);
    if (inputValue == '') {
        alert("Please write something");
    } else {
        document.getElementById("libreList").appendChild(li);
    }
    document.getElementById("listInput").value = "";
}

// Remove the last item when clicking on the remove button
function rmElement() {
    var ul = document.getElementById("libreList")
    if (countItems("libreList") == 0){
        alert("No more items to remove");
    } else {
        ul.removeChild(ul.lastChild);
    }
}

// Count elements of a list
function countItems(listID){
    var ul = document.getElementById(listID);
    var i=0, itemCount =0;
    while(ul.getElementsByTagName('li') [i++]) itemCount++;
    return itemCount;
}
